package bl.framework.testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import bl.framework.design.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
	}
	
	@Test(groups= {"smoke"})
	public void createLead() {
		System.out.println("Lead merged");
	}
}
