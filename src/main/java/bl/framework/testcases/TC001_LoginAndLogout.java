package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
	}
	@Parameters({"url","username","password"})
	@Test
	public void login(String url, String uname, String pwd) {
		startApp("chrome", url);
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, uname); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, pwd); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		/*WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);*/
	}
}








