package bl.framework.testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

@Test
public class ZoomCar extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
	}
	@Test
	public void login() {
		
		
		//Open browser
		startApp("chrome", "https://www.zoomcar.com/chennai");
		
		//Click start
		WebElement start = locateElement("class", "search");
		click(start);
		
		//Choose pickup point
		//driver.findElementByXPath("//div[@class='component-popular-locations']/div[4]").click();
		WebElement pickupPoint = locateElement("xpath", "//div[@class='component-popular-locations']/div[4]");
		click(pickupPoint);
		
		//Click next
		WebElement next = locateElement("class", "proceed");
		click(next);
		
		//To click start date
		
		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String format = sdf.format(date);
		int tomorrow=Integer.parseInt(format)+1;
		System.out.println(tomorrow);
		WebElement startDate = locateElement("xpath","//div[@class='days']/div[2]");
		click(startDate);
		
		//Click next
				WebElement next1 = locateElement("class", "proceed");
				click(next1);
		
		//Click next
				WebElement done = locateElement("class", "proceed");
				click(done);
				
		//Get list of cars
				List<WebElement> carList = locateElements("xpath","//div[@class='car-listing']");
				System.out.println("No of cars "+carList.size());
		// Get price list		
				List<WebElement> priceList = locateElements("xpath","//div[@class='price']");
		
		// Convert Web element array to String array
				List<String> str = new ArrayList<String>();
				for (WebElement ele : priceList) {
					str.add(ele.getText());
				}
				
		//Remove ?
				for (String str1 : str) {
					String replace = str1.replaceAll("\\D", "");
					System.out.println(replace);
					Integer.parseInt(str1);
				}
		
		//Sort list
				Collections.sort(str);
				System.out.println(str);
						
		
		
	}

	
}
